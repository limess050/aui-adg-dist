#!/bin/bash

# A script to check out all the currently deployed versions of aui-adg-dist and build them into the
# correct directory structure ready for deploying to the Micros CDN.

# print commands (-x) and stop execution on error (-e)
set -xe

# All 66 versions of AUI-ADG that were still being accessed as of October 2017
versions=(
    "6.0.9"
    "6.0.8"
    "6.0.7"
    "6.0.6"
    "6.0.5"
    "6.0.4"
    "6.0.3"
    "6.0.2"
    "6.0.1"
    "6.0.0"
    "5.9.8"
    "5.9.7"
    "5.9.6"
    "5.9.5"
    "5.9.4"
    "5.9.3"
    "5.9.24"
    "5.9.22"
    "5.9.21"
    "5.9.2"
    "5.9.19"
    "5.9.18"
    "5.9.17"
    "5.9.16"
    "5.9.15"
    "5.9.14"
    "5.9.13"
    "5.9.12"
    "5.9.1"
    "5.9.0"
    "5.8.9"
    "5.8.8"
    "5.8.7"
    "5.8.4"
    "5.8.3"
    "5.8.20"
    "5.8.18"
    "5.8.15"
    "5.8.14"
    "5.8.13"
    "5.8.12"
    "5.8.11"
    "5.8.10"
    "5.8.1"
    "5.8.0"
    "5.7.9"
    "5.7.8"
    "5.7.5"
    "5.7.45"
    "5.7.31"
    "5.7.3"
    "5.7.27"
    "5.7.20"
    "5.7.18"
    "5.7.16"
    "5.7.14"
    "5.7.12"
    "5.7.11"
    "5.7.1"
    "5.7.0"
    "5.6.8"
    "5.6.7"
    "5.6.16"
    "5.6.12"
    "5.6.11"
    "5.6.10"
)

git fetch git@bitbucket.org:atlassian/aui-adg-dist.git --tags

# Check all the versions exist as tags before beginning the checkout and copying process
for ver in ${versions[*]};
    do
        if GIT_DIR=./.git git show-ref --tags | egrep -q "refs/tags/$ver"
        then
            echo "Found tag '$ver'."
        else
            echo "Tag '$ver' not found, exiting!" >&2
            exit 1
        fi
    done

# Loop over each tag again, checking each out and copying to a directory structure matching the CDN
for ver in ${versions[*]};
    do
        git checkout $ver
        # make the directory matching the version
        mkdir -p "./dist/aui-adg/$ver"      

        if [ -d ./aui-next ]; then
            cp -a ./aui-next/* "./dist/aui-adg/$ver/"
        else
            cp -a ./aui/* "./dist/aui-adg/$ver/"
        fi

        echo "Copied the distribution for version '$ver'" >&2
    done

# Check if the branch is set
if [ -z "${bamboo_planRepository_branch}" ] 
then
    echo "Current build branch is not set, so using default of master"
    branch="master"
else
    branch="${bamboo_planRepository_branch}"
fi

# Now checkout the original branch again so we are back to the latest version of the service
# descriptor prior to deploy
git checkout $branch
