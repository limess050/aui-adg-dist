import { define, prop, props, h } from 'skatejs'; // eslint-disable-line
import { default as Dropdown, DropdownTriggerButton, Item as DropdownItem, events as dropdownEvents } from 'ak-dropdown';
import Button from 'ak-button';
// import TextInput from 'ak-field-text';
import { BASE_URL } from '../config';
import {
  checkAll,
  iconClick,
  versionSelected,
  resetToLatest,
  generateString,
  // setComponentsFromUrl,
  allSelected,
} from '../util/morty-helpers';

const tableStyle = require('!css!less!../css/table.less'); // eslint-disable-line
const typeStyle = require('!css!less!../css/type.less'); // eslint-disable-line
const mortyStyle = require('!css!less!../css/morty.less'); // eslint-disable-line

export default define('akr-page-component-morty', {
  props: {
    isManuallyEditing: prop.boolean(),
    components: prop.array(),
  },
  attached(elem) {
    // z-index fix
    elem.addEventListener(dropdownEvents.openAfter, (e) => {
      const dropdown = e.detail;
      dropdown.style.position = 'relative';
      dropdown.style.zIndex = '1000';
    });

    // z-index fix
    elem.addEventListener(dropdownEvents.closeAfter, (e) => {
      const dropdown = e.detail;
      dropdown.style.position = 'static';
      dropdown.style.zIndex = '0';
    });

    fetch('/atlaskit/registry/spa_data/morty_page.json')
      .then(result => result.json())
      .then((json) => {
        elem.components = json.map((c) => {
          c.selectedVersion = c.versions[0];
          return c;
        });
      });
  },
  render(elem) {
    const { components } = elem;
    if (!components.length) return <div />;

    const url = generateString(elem);
    let textInput;

    // const EditBtn = elem.isManuallyEditing ? () => null : (
    //   <Button
    //     ref={(btn => btn.addEventListener('click', () => {
    //       elem.isManuallyEditing = true;
    //       textInput.focus();
    //     }))}
    //   >{elem.isManuallyEditing ? 'Done editing' : 'Manual edit'}</Button>
    // );

    /*
    TODO: put this inside "selected-components" below once ak-field-text is fixed

    <TextInput label="Your selected components">
      <input
        type="text"
        value={url ? url.tag : ''}
        placeholder="Please pick some components to get started!"
        slot="input"
        ref={((input) => {
          textInput = input;
          textInput.addEventListener('click', () => {
            input.select();
          });
          textInput.addEventListener('blur', () => {
            setComponentsFromUrl(elem, textInput.value);
            elem.isManuallyEditing = false;
          });
        })}
        readonly={!elem.isManuallyEditing}
      />
    </TextInput>
    <div style={{ paddingTop: '42px', paddingLeft: '16px' }}>
      <EditBtn />
    </div>
    */

    return (
      <div>
        <style>
          {tableStyle.toString()}
          {typeStyle.toString()}
          {mortyStyle.toString()}
        </style>
        <h2>Components bundle</h2>
        <p>
          This tool gives you a simple bundle of any AtlasKit components you need, perfect for your
          next project or hackathon!
        </p>

        <div class="selected-components">
          <textarea
            placeholder="Please pick some components to get started!"
            ref={((input) => {
              textInput = input;
              textInput.addEventListener('click', () => {
                input.select();
              });
            })}
            readonly
          >{url ? url.tag : ''}</textarea>
        </div>

        <div class="row table-row row-header" style={{ 'font-weight': 'bold', 'margin-top': '20px' }}>
          <div class="col-6">Component</div>
          <div class="col-2">
            <ak-button onClick={resetToLatest(elem)} appearance="subtle">
                reset to latest
            </ak-button>
          </div>
          <div class={`col-2 checkbox-container ${allSelected(elem) ? 'checked' : 'not-checked'}`}>
            <ak-button onClick={checkAll(elem)} appearance="subtle">
              Check all
              <ak-icon-checkbox class="checkbox-container-icon" slot="before" />
            </ak-button>
          </div>
        </div>
        {
          components.map(component => (
            <div class="row table-row row-components" data-component={component.name}>
              <div class="col-6">
                <a href={`${BASE_URL}/${component.name}/latest/index.html`}>{component.name}</a><br />
              </div>
              <div class="col-2">
                <Dropdown data-component={component.name}>
                  <DropdownTriggerButton slot="trigger">
                    {component.selectedVersion}
                  </DropdownTriggerButton>
                  {
                    component.versions.map(v => (
                      <DropdownItem
                        active={v === component.selectedVersion}
                        onClick={versionSelected(elem, component.name, v)}
                      >{v}</DropdownItem>
                    ))
                  }
                </Dropdown>
              </div>
              <div class={`col-2 checkbox-container ${component.selected ? 'checked' : 'not-checked'}`}>
                <Button
                  onClick={iconClick(elem, component.name)}
                  appearance="subtle"
                >
                  <ak-icon-checkbox class="checkbox-container-icon" />
                </Button>
              </div>
            </div>

          ))
        }
      </div>
    );
  },
});
