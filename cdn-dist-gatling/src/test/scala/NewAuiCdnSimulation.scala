class NewAuiCdnSimulation extends AuiResourcesBase {
  override def getProtocol: String = "https"

  override def getAddress: String = "aui-cdn.prod.atl-paas.net"

  override def getResource: String = "js/aui.min.js"
}
